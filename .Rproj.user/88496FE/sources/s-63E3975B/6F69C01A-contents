---
title: "Metabarcoding workshop"
author:
  - name: Nicolas Henry
    email: nicolas.henry@sb-roscoff.fr
    affiliation: Station Biologique de Roscoff 
  - name: Cédric Berney
    email: cberney@sb-roscoff.fr
    affiliation: Station Biologique de Roscoff
  - name: Frédéric Mahé
    email: frederic.mahe@cirad.fr
    affiliation: UMR PHIM CIRAD
date: "30/03/2021"
output:
  rmdformats::robobook:
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = T,warning = F,message=F)
library(data.table)
library(magrittr)
library(metacoder)
library(leaflet)
library(rtk)
library(ggplot2)
library(vegan)
library(ggrepel)
```

# Introduction

DNA metabarcoding consists of amplifying and sequencing, using high throughput technology, a short genetic marker (metabarcode) from environmental DNA (eDNA) in order to analyse the composition of natural communities in a specific environment. Microeukaryotes play major roles within oceanic plankton ecosystems. Many of them, however, lack distinctive morphological features so molecular tools, such as metabarcoding, are essential to explore their diversity. The two most common metabarcodes used to study microeukaryotes are the hypervariable regions V4 and V9 of the 18S rRNA gene. The V4 region is located in the second quarter of the 18S rRNA gene and the V9 at the end of the 18S rRNA gene, near the ITS (internally transcribed spacer) region. For the study of microeukaryotes from samples collected during the Tara Oceans expedition, both regions have been targeted. As such, this provides a great opportunity to compare these two marker genes and define the positives and negatives of each. During this workshop, you will compare results from 81 samples collected from surface waters filtered at 0.8-2000 µm. You will investigate the taxonomic composition and highlight differences between datasets. You will next explore the main ecological patterns (alpha and beta diversity) of the planktonic microeukaryotes of the oceans and verify if the patterns are consistent between V4 and V9. The workshop will finish with a general discussion about the pros and cons of these two metabarcodes.

# Objectives

The objective of the workshop is to understand the influence of the metabarcode choice, on the ecological interpretations we can make from metabarcoding data. It will be achieved by comparing the two most common metabarcodes used to study microeukaryotes, the regions V4 and V9 of the 18S rRNA gene. You will focus on two main aspects of the data:

* The taxonomic composition: do we find the same groups with similar proportions?
* The ecological trends: are ecological patterns (alpha and beta diversity) at the community level similar?

Beyond these comparisons, the workshop also provides the opportunity to learn how to manipulate metabarcoding datasets.

# Tools

For this workshop you only need R. You will use it inside Rstudio, one of the best R integrated development environments (IDE). 

If your are connected using X2GO with MATE, you will find Rstudio here:

![You will find RStudio in Menu/Programmation/ on MATE, LXDE and probably others](rstudio_x2go.png "Title")

All the packages you need are installed on the machine you are using except perhaps metacoder. If you need to install it, run these two lines:

```{r class.source='fold-show', eval=FALSE}
BiocManager::install("biomformat")
install.packages(metacoder)
```

You can also perform all the analysis on your personal computer if you prefer.

# Material
## OTU tables

The OTU tables, one per metabarcode, were built using [Frédéric Mahé's pipeline](https://github.com/frederic-mahe/swarm/wiki/Fred's-metabarcoding-pipeline). This pipeline is characterized by the use of [Swarm](https://github.com/torognes/swarm) for clustering. For the sake of this workshop we decided to only select samples from surface waters filtered at 0.8-2000 µm. This range of fractionation allows the detection of all protists and also small animals such as copepods. This represents 81 samples out of a total of 1084.

In these tables you will find the following columns:

* amplicon: representative sequence id (sha1 hash), used as OTU id 
* taxonomy: taxonomic path until taxogroup2 (see [EukRibo section](#eukribo)); if taxogroup2 undefined taxonomic path until taxogroup1 and so on
* supergroup: supergroup from the EukRibo-specific annotation
* taxogroup1: taxogroup1 from the EukRibo-specific annotation
* taxogroup2: taxogroup2 from the EukRibo-specific annotation
* total: total number of reads in the 81 samples
* spread: number of samples the OTU appears in
* NXXXXXXXXX: sample id

An overview of the V9 OTU table:

```{r}
otu_V9 <- fread("input/18S_V9.OTU.table.gz")
otu_V9[1:10,1:9] %>% DT::datatable()
```

## EukRibo

The V4 and V9 OTUs were taxonomically assigned using the reference database EukRibo. This database corresponds to a manually curated subset of sequences from [PR2](https://github.com/pr2database/pr2database), with low sequence redundancy but high taxonomic accuracy and exhaustive coverage of the known diversity of eukaryotes. It is aimed specifically at taxonomic annotation of high-throughput metabarcoding datasets and to help discover new diversity at various taxonomic levels. A EukRibo-specific annotation into strictly monophyletic taxonomic groups at 3 levels that we called supergroup (e.g. Alveolata), taxogroup1 (e.g. Dinoflagellata), and taxogroup2 (e.g. MALV-II) was done. An OTU is assigned to a group if the best similarity score to a reference sequence in that group is higher by a certain threshold than to the best similarity score to any sequence in any other group. The thresholds we have chosen are 9% for supergroup, 6% for taxogroup1, and 3% for taxogroup2. They are somewhat empirical, based on real similarity score differences between reference sequences from different supergroups or taxogroups. This approach allows maximum confidence in the taxonomic assignment of the OTUs to the supergroups or taxogroups, without using a single universal threshold that would lead to false positives (assigned OTUs that should be considered unassignable) and false negatives (unassigned OTUs that could have been assigned).

**The OTUs not assigned to supergroups have been removed from the OTU tables.**

```{r fig.width=12,fig.height=12, fig.cap='Heat tree of the taxogroup2 with more than 30 references in EukRibo. The colour indicates the number of references.'}
obj <- readRDS("input/refs_metacoder.rds")
heat_tree(obj, 
          node_label = taxon_names,
          node_color = obj$n_obs(),
          node_color_axis_label = "# references",
          layout = "davidson-harel",
          initial_layout = "reingold-tilford",
          make_node_legend=F)

```

## Contextual data

These data describe the samples and are essential to interpret your results. In this table you will find the following columns:

* sample_id_pangaea: name of the sample, allow you to link this table with the OTU table
* station_label: label of the station, not really informative for the workshop since you have only one sample per station
* data_time: time of sampling
* event_latitude: latitude in decimal
* event_longitude: longitude in decimal
* marine_biome: Marine biome defined by (Longhurst 2007)
* ocean: Ocean and sea region (IHO General Sea Areas 1953)
* biogeographical_province: Biogeographical province (Longhurst 2007)
* temperature: water temperature (°C)
* chla: Chlorophyll a concentration (mg/m<sup>3</sup>)

An overview of the table:

```{r}
context <- fread("input/context.gz")
context %>% DT::datatable(class = "display nowrap")
```

An interactive map to give you an idea of how the samples are geographically distributed:

```{r fig.width=7,fig.height=5}
l <- leaflet() %>% setView(-4.0223789,48.7115312,3)
l %>%
  addProviderTiles(providers$Esri.WorldStreetMap) %>%
  addMiniMap(
    tiles = providers$Esri.WorldStreetMap,
    toggleDisplay = TRUE) %>%
  addCircleMarkers(data= context, ~event_longitude,~event_latitude,
                   fillOpacity = 0.8,
                   popup=~paste("station :",station_label,"<br/>","longitude :",event_longitude,
                                "<br/>","latitude :",event_latitude,"<br/>",
                                "marine_biome :",marine_biome,"<br/>","ocean :",ocean,"<br/>",
                                "province :",biogeographical_province))
```

# Rarefaction

In metabarcoding studies, the total number of reads per sample varies. The total number of reads will influence the number of OTUs you detect, essentially, the more reads you have the more OTUs you will detect. So proper comparisons based on presence or absence of OTUs/taxa can only be achieved between samples with the same total number of reads. One way to achieve the same number of reads per sample is to randomly sample, with replacement, a fixed number of reads per sample. This approach is called 'to rarefy'.

The function below allows you to rarefy your OTU tables:

```{r}
raref_DT <- function(otu_table,seq_depth,chunk_size=10,repeats=1,regex,OTU_id){
  
  sple <- otu_table[,lapply(.SD,sum),.SDcols=grep(regex,names(otu_table),value=T)] %>%
  as.matrix %>% .[1,]
  sple <- names(sple)[sple>=seq_depth]
  
  x <- split(sple, ceiling(seq_along(sple)/chunk_size))
  
  otu_table_rar <- lapply(x,function(X){
    tmp <- data.frame(otu_table[,.SD,.SDcols=c(OTU_id,X)],
                      row.names=OTU_id,check.names = F) %>%
      rtk(depth = seq_depth, repeats = repeats, ReturnMatrix = repeats, seed=1)
    tmp <- Reduce("+",tmp$raremat)/repeats
    tmp <- round(tmp) %>% data.table(keep.rownames = T) %>% melt(id.vars="rn")
    tmp[value>0]
  }) %>% rbindlist
  
  otu_table_rar_info <- otu_table_rar[,.(total=sum(value),spread=length(variable)),by=rn]
  otu_table_rar_info <- otu_table[,.SD,.SDcols= -c(grep(regex,names(otu_table),value=T),"total","spread")] %>%
    merge(otu_table_rar_info,.,by.x="rn",by.y=OTU_id)
  
  otu_table_rar <- dcast(otu_table_rar,rn~variable,value.var="value",fill=0) %>% 
    merge(otu_table_rar_info,.,by="rn")
  
  setnames(otu_table_rar,"rn",OTU_id)
  
  otu_table_rar[order(total,decreasing = T)]
  
}
```

In the example below, the V9 OTU table is rarefied at 10,000 reads

```{r class.source='fold-show'}
otu_V9_rar <- raref_DT(otu_table = otu_V9,
         seq_depth = 10000,
         regex = "^[A-Z][0-9]", # regular expression to match sample names
         OTU_id = "amplicon") # name of the column containing the OTU ids
```

If you need to calculate the total number of reads per sample, this line of code might be useful

```{r eval=FALSE}
otu_V9[,lapply(.SD,sum),.SDcols=grep("^[A-Z][0-9]",names(otu_V9),value=T)] %>%
  as.matrix %>% 
  .[1,]
```

## Exercise

**To do: Rarefy both V4 and V9 tables with the highest number of reads possible.**

# Taxonomic composition

One of the first things we do to explore the taxonomic composition of a dataset is to plot bar charts coloured by taxonomic groups. This seems pretty straightforward to achieve, however, in reality it can be hard to obtain easily readable bar charts without loosing too much information.

The visualizations offered by the library metacoder also allow exploration of the taxonomic composition of a dataset, especially when you don't know which taxonomic rank is relevant. Moreover, the package is very well [documented](https://grunwaldlab.github.io/metacoder_documentation/).

The heat tree (heat_tree function) uses the colour and size of parts of a taxonomic tree to display numeric data associated with taxa. Below, an example with the V9 dataset, with nodes coloured by the number of OTUs.

```{r fig.width=12,fig.height=12}
otu_V9_metacoder <- parse_tax_data(otu_V9_rar,
                      class_cols = "taxonomy",
                      class_regex = "^(.+)$",
                      class_sep = "|",
                      class_key = c(tax_name = "taxon_name"))

otu_V9_metacoder$data$tax_abund <- calc_taxon_abund(otu_V9_metacoder, "tax_data", cols = "total")

heat_tree(otu_V9_metacoder, 
          node_label = taxon_names,
          node_color = n_obs,
          node_color_axis_label = "# OTUs",
          layout = "davidson-harel",
          initial_layout = "reingold-tilford")
```

## Exercise

**To do: produce one heat tree per metabarcode**

**If you have time: create an additional heat tree showing the differences between the two metabarcodes**

# Alpha diversity

Alpha diversity is the mean species diversity in sites or habitats at a local scale. Two common ways to measure alpha diversity are the richness (number of species) and the entropy (Shannon index). For metabarcoding studies, the richness is assimilated as the number of OTUs.

## Richness

To compute richness, for each sample we count how many OTUs have at least one read. Here is an example with the V9 dataset. Keep in mind that if you want to be able to correctly compare your results, you must work with the rarefied table.

```{r}
otu_V9_rar[,.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,function(X) sum(X>0)) %>%
  data.table(sample_id_pangaea=names(.),richness=.,marker="V9") %>%
  head
```

## Shannon index

The Shannon index is a statistical representation of both richness and evenness. In other words, for the same number of species, a community with evenly distributed species will have a higher score than a community with few dominant species. To compute the [Shannon index](https://en.wikipedia.org/wiki/Entropy_(information_theory)) you can simply use the function diversity from the package vegan.

```{r}
otu_V9_rar[,.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,vegan::diversity) %>%
  data.table(sample_id_pangaea=names(.),shannon=.,marker="V9") %>%
  head
```

## Influence of rarefaction

The figures below are here to show you the impact of the rarefaction on these two metrics of alpha diversity. As explained before, the total number of reads influences the richness:

```{r}
rich_rar <- otu_V9_rar[,.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,function(X) sum(X>0))

rich_all <- otu_V9[,.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,function(X) sum(X>0))

rich_tot <- otu_V9[,.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,sum)

rich <- data.table(sample = names(rich_all),
           richness_all = rich_all,
           richness_rar = rich_rar,
           nreads = rich_tot)

ggplot(rich,aes(x=richness_all,y=richness_rar))+
  geom_point(aes(fill=nreads),shape=21,size=4)+
  scale_x_continuous("# OTUs before rarefaction")+
  scale_y_continuous("# OTUs after rarefaction")+
  scale_fill_viridis_c(trans="log10",option = "inferno","# reads before rarefaction")+
  theme_bw()+
  ggtitle("Effect of rarefaction (10,000 reads) on the richness")
```

In contrast, the Shannon index is not influenced by the rarefaction because it takes into account the evenness. The OTUs not retained by the rarefaction are rare and hardly influence  the index :

```{r}
div_rar <- otu_V9_rar[,.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,vegan::diversity)

div_all <- otu_V9[!is.na(supergroup),.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,vegan::diversity)

div_tot <- otu_V9[!is.na(supergroup),.SD,.SDcols=context[,sample_id_pangaea]] %>%
  apply(2,sum)

div <- data.table(sample = names(div_all),
           diversity_all = div_all,
           diversity_rar = div_rar,
           nreads = div_tot)

ggplot(div,aes(x=diversity_all,y=diversity_rar))+
  geom_point(aes(fill=nreads),shape=21,size=4)+
  scale_x_continuous("entropy before rarefaction")+
  scale_y_continuous("entropy after rarefaction")+
  scale_fill_viridis_c(trans="log10",option = "inferno","# reads before rarefaction")+
  theme_bw()+
  ggtitle("Effect of rarefaction (10,000 reads) on the entropy")
```

## Exercise

**To do: calculate the richness and the Shannon index per sample for both datasets and compare the results**

**If you have time: investigate which environmental parameters could explain a significant proportion of the alpha diversity variability**

# Beta diversity

Beta diversity is the ratio between local diversity (alpha diversity) and regional diversity (gamma diversity). Beta diversity is often estimated measuring the species composition rate of variation. In other words, it is how much the species/OTU composition between two samples varies.

## Bray-Curtis dissimilarity

We will use Bray-Curtis dissimilarity, one of the most popular dissimilarity measures in microbial ecology, to compare the samples. Even though it can be used on raw species abundances, we will first transform our (unrarefied) data into proportions because the raw number of reads is ecologically meaningless.

So the first thing to do is to transform the data:

```{r class.source='fold-show'}
otu_V9_mat_total <- otu_V9[,.SD,.SDcols=c("amplicon",context[,sample_id_pangaea])] %>%
  data.frame(row.names = "amplicon") %>%
  t %>%
  decostand(method = "total")
```

Then we compute the Bray-Curtis dissimilarity:

```{r class.source='fold-show'}
otu_V9_mat_bray <- vegdist(otu_V9_mat_total,method = "bray")
```

## Ordinations
### Principal coordinate analysis (PCoA)

One way to visualize the distances between your samples is to perform a principal coordinate analysis (PCoA). The Bray-Curtis dissimilarity is a semimetric (*i.e.* does not obey the triangle inequality -> D(a,b) + D(b,c) >= D(a,c)) and thus produces negative eigenvalues when it is used for PCoA. To avoid this behaviour, we can simply use the square root of Bray-Curtis.

```{r class.source='fold-show'}
otu_V9_mat_bray_pcoa <- sqrt(otu_V9_mat_bray) %>%
  cmdscale(k=nrow(otu_V9_mat_total)-1,
           eig = T)
colnames(otu_V9_mat_bray_pcoa$points) <- paste0("PCoA",1:ncol(otu_V9_mat_bray_pcoa$points))
```

Relative eigenvalues inform us about the percentage of variability associated with each axis of the PCoA:

```{r fig.caption='Only the first 9 axis are shown'}
eig_stats <- data.table(axis= 1:length(otu_V9_mat_bray_pcoa$eig),
           otu_V9_mat_bray_pcoa$eig*100 / sum(otu_V9_mat_bray_pcoa$eig))

ggplot(eig_stats[1:9],aes(x=paste("PCoA",axis),y=V2))+
  geom_col()+
  scale_x_discrete("")+
  scale_y_continuous("% variability")+
  theme_bw()
```

Almost 10% of the variability between samples is associated with the first axis and 5.1% with the second. Along the first axis we can see a clear separation between temperate/tropical and polar samples. On the second axis, a group of samples (stations 114 to 118) stand out from the rest of the samples. They are planktonic samples from coral reefs whereas most of the other samples are far from coasts. So these differences are not surprising. 

```{r fig.width=8, fig.height=7}
otu_V9_mat_bray_pcoa_points <- data.table(otu_V9_mat_bray_pcoa$points[,1:2],keep.rownames = T)
setnames(otu_V9_mat_bray_pcoa_points,"rn","sample_id_pangaea")
otu_V9_mat_bray_pcoa_points <- merge(otu_V9_mat_bray_pcoa_points,context,by="sample_id_pangaea")

ggplot(otu_V9_mat_bray_pcoa_points,aes(x=PCoA1,y=PCoA2))+
  geom_point(aes(fill=temperature),shape=21,size=3)+
  geom_text_repel(aes(label=ifelse(PCoA2 < -0.4, station_label,NA)))+
  scale_x_continuous(paste0("PCoA1: ",round(eig_stats[1,V2],digits = 1),"%"))+
  scale_y_continuous(paste0("PCoA2: ",round(eig_stats[2,V2],digits = 1),"%"))+
  scale_fill_viridis_c("Temperature (°C)")+
  theme_bw()+
  coord_fixed()
```

### Non-metric multidimensional scaling (nMDS)

nMDS is often used in microbial ecology. It has the advantage of representing your objects (samples) in a small and specified number of dimensions whereas eigenvector-based methods such as PCA, PCoA and CA produce an ordination of the objects in full dimensional space. This approach is convenient to have a preliminary understanding of how the objects differ to each other, however, it does not preserve the exact distances between the objects. It is therefore almost impossible, with nMDS, to accurately estimate the importance of the different ecological gradients within your data.

```{r results=F}
otu_V9_mat_bray_nmds <- metaMDS(otu_V9_mat_total,
                                k=2,
                                autotransform = F,
                                distance = "bray",
                                wascores = F,
                                try = 10)
```

We retrieve, using nMDS, the same general pattern as with the PCoA, polar samples are different from the rest of the samples. We also have some outliers such as the sample from station 68.

```{r fig.width=8, fig.height=7}
otu_V9_mat_bray_nmds_points <- data.table(otu_V9_mat_bray_nmds$points,keep.rownames = T)
setnames(otu_V9_mat_bray_nmds_points,"rn","sample_id_pangaea")
otu_V9_mat_bray_nmds_points <- merge(otu_V9_mat_bray_nmds_points,context,by="sample_id_pangaea")

ggplot(otu_V9_mat_bray_nmds_points,aes(x=MDS1,y=MDS2))+
  geom_point(aes(fill=temperature),shape=21,size=3)+
  scale_fill_viridis_c("Temperature (°C)")+
  geom_text_repel(aes(label=ifelse(MDS2 < -1 | MDS1 < -1, station_label,NA)))+
  theme_bw()+
  coord_fixed()
```

One way to know to what extent the original distances are preserved in the results of the nMDS is to look at the stress value. This value is the sum of the squared differences between the fitted values and the corresponding values forecasted by the regression function applied between the fitted and the original distances (see Sheppard diagram below). 

From https://mb3is.megx.net/gustame/dissimilarity-based-methods/nmds

> A stress value around or above 0.2 is deemed suspect and a stress value approaching 0.3 indicates that the ordination is arbitrary. Stress values equal to or below 0.1 are considered fair, while values equal to or below 0.05 indicate a good fit. Allowing the algorithm to ordinate in more dimensions can reduce the stress; however, allowing more than 3 dimensions quickly makes interpretation more challenging.

To have a better understanding of the quality of the nMDS you can plot a Sheppard diagram which compares the fitted distances (distances in the nMDS) to the original ones (Bray-Curtis dissimilarity).

```{r fig.width=8, fig.height=7}

monoreg <- isotone::gpava(otu_V9_mat_bray_nmds$diss,otu_V9_mat_bray_nmds$dist)
monoreg <- data.table(monoreg$z,monoreg$x)

ggplot(data.table(otu_V9_mat_bray_nmds$diss,otu_V9_mat_bray_nmds$dist),aes(x=V1,y=V2))+
  geom_hex(alpha=0.6)+
  geom_line(data=monoreg,aes(group=""))+
  scale_fill_viridis_c()+
  scale_x_continuous("Bray-Curtis dissimilarity")+
  scale_y_continuous("Fitted distances in 2D nMDS")+
  theme_bw()
```

## Exercise

**To do: run one ordination of your choice (nMDS or PCoA) on both datasets and compare the results**

**If you have time: test for beta diversity similarity using the [Mantel test](https://mb3is.megx.net/gustame/hypothesis-tests/the-mantel-test) and a [Procrustes analysis](https://mb3is.megx.net/gustame/other-methods/procrustes-analysis)**

# Solutions to exercises

Solutions to exercises are [here](solutions.html) 

# Session info
```{r}
sessionInfo()
```
